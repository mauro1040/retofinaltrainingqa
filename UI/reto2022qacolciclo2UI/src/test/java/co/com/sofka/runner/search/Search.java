package co.com.sofka.runner.search;

import io.cucumber.junit.CucumberOptions;
import io.cucumber.junit.CucumberSerenityRunner;
import org.junit.runner.RunWith;

@RunWith(CucumberSerenityRunner.class)
@CucumberOptions(
        snippets = CucumberOptions.SnippetType.CAMELCASE,
        features = {"src/test/resources/features/search.feature"},
        glue = {"co.com.sofka.stepdefinition.searchstepdefinition"},
        tags = "not @ignore"
)

public class Search {
}
