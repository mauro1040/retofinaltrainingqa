package co.com.sofka.stepdefinition.searchstepdefinition;

import co.com.sofka.setup.Setup;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;

import static co.com.sofka.questions.search.SearchSuccess.searchSuccess;
import static co.com.sofka.task.landingpage.OpenLandingPage.openLandingPage;
import static co.com.sofka.task.searchfieldform.SearchFieldGreen.searchFieldGreen;
import static co.com.sofka.task.searchfieldform.SearchFieldPurple.searchFieldPurple;
import static co.com.sofka.utils.Enumerates.ACTOR_NAME;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.Matchers.equalTo;


public class SearchFormStepdefinition extends Setup {

    @Dado("que el usuario se encuentra en el sitio web principal en traveler")
    public void queElUsuarioSeEncuentraEnElSitioWebPrincipalEnTraveler() {
        actorSetupTheBrowsers(ACTOR_NAME.getValue());
        theActorInTheSpotlight().wasAbleTo(
                openLandingPage()
        );
    }
    @Cuando("El cliente selecciona color green")
    public void elClienteSeleccionaColorGreen() {
        theActorInTheSpotlight().attemptsTo(
                searchFieldGreen()
        );
    }
    @Entonces("El cliente podra visualizar y reservar destinos solo con este color")
    public void elClientePodraVisualizarYReservarDestinosSoloConEsteColor() {
        theActorInTheSpotlight().should(
                seeThat(
                        searchSuccess(), equalTo(true)
                )
        );
    }

    @Cuando("El cliente selecciona color purple")
    public void elClienteSeleccionaColorPurple() {
        theActorInTheSpotlight().attemptsTo(
                searchFieldPurple()
        );
    }
    @Entonces("El cliente podra realizar reservas solo con color purple")
    public void elClientePodraRealizarReservasSoloConColorPurple() {
        theActorInTheSpotlight().should(
                seeThat(
                        searchSuccess(), equalTo(true)
                )
        );
    }

}
