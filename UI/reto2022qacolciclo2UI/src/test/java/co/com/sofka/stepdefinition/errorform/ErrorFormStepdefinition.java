package co.com.sofka.stepdefinition.errorform;

import co.com.sofka.setup.Setup;
import com.github.javafaker.Faker;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;

import static co.com.sofka.questions.buyForm.FillFieldsForm.fillFieldsForm;
import static co.com.sofka.questions.buyForm.FillFieldsFormEmail.fillFieldsFormEmail;
import static co.com.sofka.task.FillAllFieldsForm.FillAllFieldsForm.fillAllFieldsForm;
import static co.com.sofka.task.landingpage.OpenLandingPage.openLandingPage;
import static co.com.sofka.utils.Enumerates.ACTOR_NAME;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.Matchers.equalTo;

public class ErrorFormStepdefinition extends Setup {

    Faker faker = new Faker();
    @Dado("que el usuario esta en la seccion de completar el formulario de compra")
    public void queElUsuarioEstaEnLaSeccionDeCompletarElFormularioDeCompra() {
        actorSetupTheBrowsers(ACTOR_NAME.getValue());
        theActorInTheSpotlight().wasAbleTo(
                openLandingPage()
        );

    }
    @Cuando("el usuario completa los datos")
    public void elUsuarioCompletaLosDatos() {
        theActorInTheSpotlight().attemptsTo(
                fillAllFieldsForm()
                        .setName(faker.name().firstName())
                        .setEmail(faker.internet().emailAddress())
                        .setSocialSecurityNumber(faker.idNumber().invalidSvSeSsn())
                        .setPhone(faker.phoneNumber().phoneNumber())
        );
    }
    @Entonces("se muestra el error en el campo de seguro social.")
    public void seMuestraElErrorEnElCampoDeSeguroSocial() {
        theActorInTheSpotlight().should(
                seeThat(
                        fillFieldsForm()
                                .is(), equalTo(true)
                )
        );
    }

    @Cuando("el usuario completa todos los datos solicitados")
    public void elUsuarioCompletaTodosLosDatosSolicitados() {
        theActorInTheSpotlight().attemptsTo(
                fillAllFieldsForm()
                        .setName(faker.name().firstName())
                        .setEmail(faker.name().firstName())
                        .setSocialSecurityNumber(faker.idNumber().valid())
                        .setPhone(faker.phoneNumber().phoneNumber())
        );
    }
    @Entonces("se muestra el error en el campo de email.")
    public void seMuestraElErrorEnElCampoDeEmail() {
        theActorInTheSpotlight().should(
                seeThat(
                        fillFieldsFormEmail()
                                .is(), equalTo(true)
                )
        );
    }
}
