package co.com.sofka.runner.Login;

import io.cucumber.junit.CucumberOptions;
import io.cucumber.junit.CucumberSerenityRunner;
import org.junit.runner.RunWith;

@RunWith(CucumberSerenityRunner.class)
@CucumberOptions(
        snippets = CucumberOptions.SnippetType.CAMELCASE,
        features = {"src/test/resources/features/login/loginSuccess.feature"},
        glue = {"co.com.sofka.stepdefinition.loginsuccess"},
        tags = "not @ignore"
)
public class LoginSuccessTest {

}
