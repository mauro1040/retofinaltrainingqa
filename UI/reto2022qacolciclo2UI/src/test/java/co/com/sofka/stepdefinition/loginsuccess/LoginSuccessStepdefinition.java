package co.com.sofka.stepdefinition.loginsuccess;

import co.com.sofka.setup.Setup;
import com.github.javafaker.Faker;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;

public class LoginSuccessStepdefinition extends Setup {

    Faker faker = new Faker();
    @Dado("que el usuario navega hasta la pagina de inicio")
    public void queElUsuarioNavegaHastaLaPaginaDeInicio() {

    }
    @Cuando("el usuario completa los campos correctamente")
    public void elUsuarioCompletaLosCamposCorrectamente() {

    }
    @Entonces("se muestra el nombre del usuario en la barra de navegacion.")
    public void seMuestraElNombreDelUsuarioEnLaBarraDeNavegacion() {

    }
}
