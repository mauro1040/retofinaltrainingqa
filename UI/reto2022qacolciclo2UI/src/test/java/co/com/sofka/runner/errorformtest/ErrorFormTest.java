package co.com.sofka.runner.errorformtest;

import io.cucumber.junit.CucumberOptions;
import io.cucumber.junit.CucumberSerenityRunner;
import org.junit.runner.RunWith;

@RunWith(CucumberSerenityRunner.class)
@CucumberOptions(
        snippets = CucumberOptions.SnippetType.CAMELCASE,
        features = {"src/test/resources/features/errorForm.feature"},
        glue = {"co.com.sofka.stepdefinition.errorform"},
        tags = "not @ignore"
)
public class ErrorFormTest {
}
