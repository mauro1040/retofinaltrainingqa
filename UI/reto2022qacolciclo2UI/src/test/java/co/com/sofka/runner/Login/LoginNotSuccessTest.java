package co.com.sofka.runner.Login;

import io.cucumber.junit.CucumberOptions;
import io.cucumber.junit.CucumberSerenityRunner;
import org.junit.runner.RunWith;

@RunWith(CucumberSerenityRunner.class)
@CucumberOptions(
        snippets = CucumberOptions.SnippetType.CAMELCASE,
        features = {"src/test/resources/features/login/loginNotSuccess.feature"},
        glue = {"co.com.sofka.stepdefinition.loginnotsuccess"},
        tags = "not @ignore"
)
public class LoginNotSuccessTest {
}
