package co.com.sofka.stepdefinition.loginnotsuccess;

import co.com.sofka.setup.Setup;
import com.github.javafaker.Faker;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;

import static co.com.sofka.questions.login.NotSuccessLogin.notSuccessLogin;
import static co.com.sofka.task.landingpage.OpenLandingPage.openLandingPage;
import static co.com.sofka.task.loginFillFields.LoginFillFields.loginFillFields;
import static co.com.sofka.utils.Enumerates.ACTOR_NAME;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.Matchers.equalTo;

public class LoginNotSuccessStepdefinition extends Setup {

    Faker faker = new Faker();
    @Dado("que el usuario esta ingresa a la pagina")
    public void queElUsuarioEstaIngresaALaPagina() {
        actorSetupTheBrowsers(ACTOR_NAME.getValue());
        theActorInTheSpotlight().wasAbleTo(
                openLandingPage()
        );
    }
    @Cuando("el usuario completa los datos y confirma")
    public void elUsuarioCompletaLosDatosYConfirma() {
        theActorInTheSpotlight().attemptsTo(
                loginFillFields()
                        .setUserName(faker.name().name())
        );
    }
    @Entonces("se muestra el error en el campo correspondiente.")
    public void seMuestraElErrorEnElCampoCorrespondiente() {
        theActorInTheSpotlight().should(
                seeThat(
                        notSuccessLogin(), equalTo(true)
                )

        );
    }
}
