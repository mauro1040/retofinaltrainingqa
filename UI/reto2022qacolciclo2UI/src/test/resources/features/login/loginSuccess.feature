# language: es

Característica: validacion de usuario
  Como usuario quiero ingresar con mis datos correctos

  Escenario: Usuario registrado completa el formulario e ingresa a su sesion
    Dado que el usuario navega hasta la pagina de inicio
    Cuando el usuario completa los campos correctamente
    Entonces se muestra el nombre del usuario en la barra de navegacion.

