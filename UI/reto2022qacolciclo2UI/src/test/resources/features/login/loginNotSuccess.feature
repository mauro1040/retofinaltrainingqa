# language: es

Característica: validacion de errores en el login
  Como administrador quiere validar los campos obligatorios del formulario login.

  Escenario: Usuario no coloca la contrasena
    Dado que el usuario esta ingresa a la pagina
    Cuando el usuario completa los datos y confirma
    Entonces se muestra el error en el campo correspondiente.

