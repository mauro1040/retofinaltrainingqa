package co.com.sofka.userinterfaces.search;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

import static org.openqa.selenium.By.xpath;

public class SearchFieldForm extends PageObject {

    public static final Target PLANET_COLOR = Target
            .the("planetColor")
            .located(xpath("/html/body/div/div/section[2]/div[3]/div/div/div[2]/div/input"));


    public static final Target PLANET_GREEN = Target
            .the("planetGreen")
            .located(xpath("/html/body/div/div/section[2]/div[3]/div/div/div[2]/ul/li[2]"));

    public static final Target PLANET_PURPLE = Target
            .the("planetPurple")
            .located(xpath("/html/body/div/div/section[2]/div[3]/div/div/div[2]/ul/li[6]"));


    public static final Target BOOK_BOTTON = Target
            .the("bookBotton")
            .located(xpath("/html/body/div/div/section[2]/div[4]/div/div/div[1]/div[4]/button"));


    public static final Target VALIDATION_BOOKED = Target
            .the("bookBotton")
            .located(xpath("/html/body/div/div/section[2]/div[4]/div/div/div[1]/div[4]/button"));


}
