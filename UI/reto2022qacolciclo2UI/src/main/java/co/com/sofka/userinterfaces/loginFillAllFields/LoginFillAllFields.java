package co.com.sofka.userinterfaces.loginFillAllFields;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

import static org.openqa.selenium.By.xpath;

public class LoginFillAllFields extends PageObject {

    public static final Target LOGIN_BOTTON = Target
            .the("loginBotton")
            .located(xpath("//*[@id=\"app\"]/div/header/div/div[2]/ul/li/button"));

    public static final Target USER_NAME = Target
            .the("userName")
            .located(xpath("//*[@id=\"login\"]/div[1]/input"));

    public static final Target USER_PASSWORD = Target
            .the("userPassword")
            .located(xpath("//*[@id=\"login\"]/div[2]/input"));

    public static final Target SUBMIT_BUTTON = Target
            .the("submitButton")
            .located(xpath("//*[@id=\"app\"]/div/section[3]/div/div[2]/div/nav/button[2]"));

    public static final Target VALIDATION_SUCCESS = Target
            .the("validationSuccess")
            .located(xpath("//*[@id=\"app\"]/div/header/div/div[2]/ul/div/button/span[1]"));

    public static final Target VALIDATION_ERROR = Target
            .the("validationError")
            .located(xpath("//*[@id=\"login\"]/div[2]/span[3]"));
}
