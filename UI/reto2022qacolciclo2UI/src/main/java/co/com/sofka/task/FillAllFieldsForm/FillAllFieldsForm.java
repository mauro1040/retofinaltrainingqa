package co.com.sofka.task.FillAllFieldsForm;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.Scroll;

import static co.com.sofka.userinterfaces.fillAllFieldsForm.FillAllFieldsForm.*;

public class FillAllFieldsForm implements Task {

    private String name;
    private String email;
    private String socialSecurityNumber;
    private String phone;

    public FillAllFieldsForm setName(String name) {
        this.name = name;
        return this;
    }

    public FillAllFieldsForm setEmail(String email) {
        this.email = email;
        return this;
    }

    public FillAllFieldsForm setSocialSecurityNumber(String socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
        return this;
    }

    public FillAllFieldsForm setPhone(String phone) {
        this.phone = phone;
        return this;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(

                Scroll.to(ELEMENTS),
                Click.on(ELEMENTS),

                Scroll.to(WRITE_NAME),
                Enter.theValue(name).into(WRITE_NAME),

                Scroll.to(WRITE_EMAIL),
                Enter.theValue(email).into(WRITE_EMAIL),

                Scroll.to(WRITE_SSN),
                Enter.theValue(socialSecurityNumber).into(WRITE_SSN),

                Scroll.to(WRITE_PHONE),
                Enter.theValue(phone).into(WRITE_PHONE)


        );
    }

    public static FillAllFieldsForm fillAllFieldsForm() {return new FillAllFieldsForm();}


}
