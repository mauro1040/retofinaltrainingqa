package co.com.sofka.userinterfaces.fillAllFieldsForm;

import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.pages.PageObject;

import static org.openqa.selenium.By.xpath;

public class FillAllFieldsForm extends PageObject {

    public static final Target ELEMENTS = Target
            .the("selectElements")
            .located(xpath("/html/body/div/div/section[2]/div[4]/div/div/div[3]/div[4]/button"));

    public static final Target WRITE_NAME = Target
            .the("writeName")
            .located(xpath("//*[@id=\"app\"]/div/div[2]/section[1]/div[3]/div[1]/form/div[1]/input"));

    public static final Target WRITE_EMAIL = Target
            .the("writeEmail")
            .located(xpath("//*[@id=\"app\"]/div/div[2]/section[1]/div[3]/div[1]/form/div[2]/input"));

    public static final Target WRITE_SSN = Target
            .the("writeSocialSecurityNumber")
            .located(xpath("//*[@id=\"app\"]/div/div[2]/section[1]/div[3]/div[1]/form/div[3]/input"));

    public static final Target WRITE_PHONE = Target
            .the("writePhone")
            .located(xpath("//*[@id=\"app\"]/div/div[2]/section[1]/div[3]/div[1]/form/div[4]/input"));

    public static final Target VALIDATION_EMAIL = Target
            .the("validationEmail")
            .located(xpath("//*[@id=\"app\"]/div/div[2]/section[1]/div[3]/div[1]/form/div[2]/span[3]"));

    public static final Target VALIDATION_SSN = Target
            .the("validationSocialSecurityNumber")
            .located(xpath("//*[@id=\"app\"]/div/div[2]/section[1]/div[3]/div[1]/form/div[3]/span[3]"));
}
