package co.com.sofka.questions.search;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static co.com.sofka.userinterfaces.search.SearchFieldForm.VALIDATION_BOOKED;

public class SearchSuccess implements Question<Boolean> {

    private String messenge = "BOOKED";

    @Override
    public Boolean answeredBy(Actor actor) {
        return (VALIDATION_BOOKED.resolveFor(actor).getText().contains(messenge));
    }

    public static SearchSuccess searchSuccess(){return new SearchSuccess();}
}
