package co.com.sofka.task.searchfieldform;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Scroll;

import static co.com.sofka.userinterfaces.search.SearchFieldForm.*;

public class SearchFieldGreen implements Task {

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(

                Scroll.to(PLANET_COLOR),
                Click.on(PLANET_COLOR),

                Click.on(PLANET_GREEN),
                Scroll.to(BOOK_BOTTON),
                Click.on(BOOK_BOTTON),
                Scroll.to(VALIDATION_BOOKED)

        );
    }

    public static SearchFieldGreen searchFieldGreen(){
        return new SearchFieldGreen();
    }
}
