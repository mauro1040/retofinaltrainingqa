package co.com.sofka.questions.login;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static co.com.sofka.userinterfaces.loginFillAllFields.LoginFillAllFields.VALIDATION_ERROR;

public class NotSuccessLogin implements Question<Boolean> {

    @Override
    public Boolean answeredBy(Actor actor) {
        String messenge = "Password is a required field.";
        return (VALIDATION_ERROR.resolveFor(actor).getText().contains(messenge));
    }

    public static NotSuccessLogin notSuccessLogin() {return new NotSuccessLogin();}
}
