package co.com.sofka.questions.buyForm;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static co.com.sofka.userinterfaces.fillAllFieldsForm.FillAllFieldsForm.VALIDATION_EMAIL;

public class FillFieldsFormEmail implements Question<Boolean> {

    private final String messenge = "Enter a valid e-mail address.";

    public FillFieldsFormEmail is() {return this;}

    @Override
    public Boolean answeredBy(Actor actor) {
        return (VALIDATION_EMAIL.resolveFor(actor).getText().contains(messenge));
    }

    public static FillFieldsFormEmail fillFieldsFormEmail() {return new FillFieldsFormEmail();}
}