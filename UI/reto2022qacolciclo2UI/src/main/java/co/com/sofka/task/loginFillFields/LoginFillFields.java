package co.com.sofka.task.loginFillFields;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.Scroll;

import static co.com.sofka.userinterfaces.loginFillAllFields.LoginFillAllFields.*;

public class LoginFillFields implements Task {

    private String userName;
    private String userPassword;

    public LoginFillFields setUserName(String userName) {
        this.userName = userName;
        return this;
    }

    public LoginFillFields setUserPassword(String userPassword) {
        this.userPassword = userPassword;
        return this;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Scroll.to(LOGIN_BOTTON),
                Click.on(LOGIN_BOTTON),

                Scroll.to(USER_NAME),
                Enter.theValue(userName).into(USER_NAME),

                Scroll.to(USER_PASSWORD),
                Enter.theValue(userPassword).into(USER_PASSWORD),

                Scroll.to(SUBMIT_BUTTON),
                Click.on(SUBMIT_BUTTON)
        );
    }

    public static LoginFillFields loginFillFields() {return new LoginFillFields();}
}
