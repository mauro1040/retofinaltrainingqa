package co.com.sofka.questions.buyForm;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static co.com.sofka.userinterfaces.fillAllFieldsForm.FillAllFieldsForm.VALIDATION_SSN;

public class FillFieldsForm implements Question<Boolean> {

    private final String messenge = "Enter a valid Social Security number (xxx-xx-xxxx).";

    public FillFieldsForm is() {return this;}

    @Override
    public Boolean answeredBy(Actor actor) {
        return (VALIDATION_SSN.resolveFor(actor).getText().contains(messenge));
    }

    public static FillFieldsForm fillFieldsForm() {return new FillFieldsForm();}
}
