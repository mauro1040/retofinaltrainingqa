package co.com.sofka.questions.login;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static co.com.sofka.userinterfaces.loginFillAllFields.LoginFillAllFields.VALIDATION_SUCCESS;

public class SuccessLogin implements Question<Boolean> {

    @Override
    public Boolean answeredBy(Actor actor) {
        String messenge = "HELLO, JOHN";
        return (VALIDATION_SUCCESS.resolveFor(actor).getText().contains(messenge));
    }

    public static SuccessLogin successLogin(){return new SuccessLogin();}
}
