package co.com.sofka.task.searchfieldform;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Scroll;

import static co.com.sofka.userinterfaces.search.SearchFieldForm.*;

public class SearchFieldPurple implements Task {

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(

                Scroll.to(PLANET_COLOR),
                Click.on(PLANET_COLOR),

                Scroll.to(PLANET_PURPLE),
                Click.on(PLANET_PURPLE),

                Scroll.to(BOOK_BOTTON),
                Click.on(BOOK_BOTTON),
                Scroll.to(VALIDATION_BOOKED)

        );
    }

    public static SearchFieldPurple searchFieldPurple(){
        return new SearchFieldPurple();
    }

}
