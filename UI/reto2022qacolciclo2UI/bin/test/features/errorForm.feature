# language: es

Característica: validacion de errores en el form de compra
  Como administrador quiere validar los campos obligatorios del formulario.

  Antecedentes:
  Dado que el usuario esta en la seccion de completar el formulario de compra

  Escenario: Usuario no registrado completa mal el numero de seguridad social
    Cuando el usuario completa los datos
    Entonces se muestra el error en el campo de seguro social.

  Escenario: Usuario no registrado completa mal el campo email
    Cuando el usuario completa todos los datos solicitados
    Entonces se muestra el error en el campo de email.