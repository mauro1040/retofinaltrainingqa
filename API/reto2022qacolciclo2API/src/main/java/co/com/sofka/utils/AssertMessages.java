package co.com.sofka.utils;

public enum AssertMessages {

    USER_UPDATE("maohvv14");

    private final String value;

    public String getValue() {
        return value;
    }

    AssertMessages(String value) {
        this.value = value;
    }
}
