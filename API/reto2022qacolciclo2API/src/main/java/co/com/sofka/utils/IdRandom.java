package co.com.sofka.utils;


import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;

public class IdRandom {
    private IdRandom()  {

    }

    public static String pathIdGenerate() throws NoSuchAlgorithmException {
        Random rand = SecureRandom.getInstanceStrong();

        int numRandom = rand.nextInt(9);

        if(numRandom == 0){
            return (""+1);
        }else{
            return (""+numRandom);
        }
    }


}
