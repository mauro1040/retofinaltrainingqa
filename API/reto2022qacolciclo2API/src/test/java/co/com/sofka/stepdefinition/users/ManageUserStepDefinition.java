package co.com.sofka.stepdefinition.users;

import co.com.sofka.setup.SetUp;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import net.serenitybdd.screenplay.rest.questions.LastResponse;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import org.assertj.core.api.Assertions;

import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;

import static co.com.sofka.questions.common.ResponseCode.was;
import static co.com.sofka.questions.common.ReturnStringValue.returnStringValue;
import static co.com.sofka.tasks.DoDelete.doDelete;
import static co.com.sofka.tasks.DoPut.doPut;
import static co.com.sofka.utils.AssertMessages.USER_UPDATE;
import static co.com.sofka.utils.FileUtilities.readFile;
import static co.com.sofka.utils.IdRandom.pathIdGenerate;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;

public class ManageUserStepDefinition extends SetUp {

    private static final Logger LOGGER = Logger.getLogger(ManageUserStepDefinition.class);
    private static final String BODY_MESSAGE_JSON ="src/test/resources/files/users/user.json";

    private final HashMap<String, Object> headers = new HashMap<>();

    private String bodyMessageRequest;

    private String id;

    private String defineBodyMessageRequest(){
        return readFile(BODY_MESSAGE_JSON);
    }

    @Dado("que el administrador se encuentra usando el recurso Users del sitio SwaggerUI")
    public void queElAdministradorSeEncuentraUsandoElRecursoUsersDelSitioSwaggerUI() {
        try {
            String admin = "Admin";
            generalSetUp(admin);
            headers.put("Content-type","application/json; charset=UTF-8");
            bodyMessageRequest = defineBodyMessageRequest();
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            Assertions.fail(e.getMessage());
        }
    }
    @Cuando("el administrador hace una solicitud para eliminar un usuario registrado por Id")
    public void elAdministradorHaceUnaSolicitudParaEliminarUnUsuarioRegistradoPorId() throws NoSuchAlgorithmException {
        id = pathIdGenerate();
        try {
            theActorInTheSpotlight().attemptsTo(
                    doDelete().usingTheResource(RESOURCE_USERS  + id)
                            .withHeaders(headers)
            );
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            Assertions.fail(e.getMessage());
        }
    }
    @Entonces("el administrador recibira una confirmacion exitosa del proceso")
    public void elAdministradorRecibiraUnaConfirmacionExitosaDelProceso() {
        theActorInTheSpotlight().should(
                seeThat("el código de respuesta",was(),equalTo(HttpStatus.SC_OK))
        );
    }

    @Cuando("el administrador hace una solicitud para actualizar los datos de un usuario")
    public void elAdministradorHaceUnaSolicitudParaActualizarLosDatosDeUnUsuario() throws NoSuchAlgorithmException {
        id = pathIdGenerate();
        try {
            theActorInTheSpotlight().attemptsTo(
                    doPut().usingTheResource(RESOURCE_USERS  + id)
                            .withHeaders(headers)
                            .andBodyRequest(bodyMessageRequest)
            );
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            Assertions.fail(e.getMessage());
        }
    }
    @Entonces("el administrador recibira confirmacion del proceso con los datos actualizados")
    public void elAdministradorRecibiraConfirmacionDelProcesoConLosDatosActualizados() {
        String restResponse=new String(LastResponse.received().answeredBy(theActorInTheSpotlight()).asByteArray(), StandardCharsets.UTF_8);

        theActorInTheSpotlight().should(
                seeThat("el código de respuesta",was(),equalTo(HttpStatus.SC_OK))
        );

        theActorInTheSpotlight().should("El usuario modificado corresponde con maohvv14",
                seeThat(returnStringValue(restResponse),containsString(USER_UPDATE.getValue()))
        );
    }
}
