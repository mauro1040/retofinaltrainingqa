package co.com.sofka.stepdefinition.authors.createauthor;

import co.com.sofka.setup.SetUp;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import net.serenitybdd.screenplay.rest.questions.LastResponse;
import org.apache.http.HttpStatus;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import static co.com.sofka.questions.common.ResponseCode.was;
import static co.com.sofka.questions.common.ReturnStringValue.returnStringValue;
import static co.com.sofka.tasks.DoPost.doPost;
import static co.com.sofka.utils.FileUtilities.readFile;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;

public class CreateAuthorStepDefinition extends SetUp {

    private final HashMap<String,Object> headers=new HashMap<>();
    private static final String ROL_NAME= "Editor";
    private static final String CREATE_AUTHOR_JSON="src/test/resources/files/authors/createAuthor.json";
    private String bodyRequest;

    @Dado("que el editor navegó hasta la sección de registro de autor y suministre los datos")
    public void queElEditorNavegoHastaLaSeccionDeRegistroDeAutorYSuministreLosDatos() {
        generalSetUp(ROL_NAME);
        headers.put("Content-type","application/json");
        bodyRequest=defineBodyRequest();

    }
    @Cuando("el editor realice la petición de crear registro de autor")
    public void elEditorRealiceLaPeticionDeCrearRegistroDeAutor() {
        theActorInTheSpotlight().attemptsTo(
                doPost().usingTheResource(RESOURCE_AUTHORS)
                        .withHeaders(headers)
                        .andBodyRequest(bodyRequest)
        );
    }
    @Entonces("visualizará que se creo correctamente el registro de autor")
    public void visualizaraQueSeCreoCorrectamenteElRegistroDeAutor() {

        String restResponse=new String(LastResponse.received().answeredBy(theActorInTheSpotlight()).asByteArray(), StandardCharsets.UTF_8);

        theActorInTheSpotlight().should(
                seeThat("el código de respuesta",was(),equalTo(HttpStatus.SC_OK))
        );

        theActorInTheSpotlight().should("El nombre del autor registrado es Estefania",
                seeThat(returnStringValue(restResponse),containsString("Estefania"))

        );
    }

    private String defineBodyRequest(){
        return readFile(CREATE_AUTHOR_JSON);
    }
}
