package co.com.sofka.stepdefinition.authors.updateauthor;

import co.com.sofka.setup.SetUp;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import net.serenitybdd.screenplay.rest.questions.LastResponse;
import org.apache.http.HttpStatus;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import static co.com.sofka.questions.common.ResponseCode.was;
import static co.com.sofka.questions.common.ReturnStringValue.returnStringValue;
import static co.com.sofka.tasks.DoPut.doPut;
import static co.com.sofka.utils.FileUtilities.readFile;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;

public class UpdateAuthorStepDefinition extends SetUp {
    private final HashMap<String,Object> headers=new HashMap<>();
    private static final String ROL_NAME= "Editor";
    private static final String UPDATE_AUTHOR_JSON="src/test/resources/files/authors/updateAuthor.json";
    private String bodyRequest;
    @Dado("que el editor navegó hasta la sección de actualización de registros de autor y suministre los datos")
    public void queElEditorNavegoHastaLaSeccionDeActualizacionDeRegistrosDeAutorYSuministreLosDatos() {
        generalSetUp(ROL_NAME);
        headers.put("Content-type","application/json");
        bodyRequest=defineBodyRequest();
    }
    @Cuando("el editor realice la petición de actualizar registro de autor")
    public void elEditorRealiceLaPeticionDeActualizarRegistroDeAutor() {
        theActorInTheSpotlight().attemptsTo(
                doPut()
                        .usingTheResource(RESOURCE_AUTHORS+"1")
                        .withHeaders(headers)
                        .andBodyRequest(bodyRequest)
        );
    }
    @Entonces("visualizará que se creo actualizo correctamente el registro del autor")
    public void visualizaraQueSeCreoActualizoCorrectamenteElRegistroDelAutor() {
        String restResponse=new String(LastResponse.received().answeredBy(theActorInTheSpotlight()).asByteArray(), StandardCharsets.UTF_8);

        theActorInTheSpotlight().should(
                seeThat("el código de respuesta",was(),equalTo(HttpStatus.SC_OK))
        );

        theActorInTheSpotlight().should("El nombre del autor registrado es Estefania",
                seeThat(returnStringValue(restResponse),containsString("Estefania Actualizado"))
        );
    }

    private String defineBodyRequest(){
        return readFile(UPDATE_AUTHOR_JSON);
    }
}
