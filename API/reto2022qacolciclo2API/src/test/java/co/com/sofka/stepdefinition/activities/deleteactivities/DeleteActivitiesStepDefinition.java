package co.com.sofka.stepdefinition.activities.deleteactivities;

import co.com.sofka.setup.SetUp;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import org.apache.http.HttpStatus;
import java.util.HashMap;
import static co.com.sofka.questions.common.ResponseCode.was;
import static co.com.sofka.tasks.DoDelete.doDelete;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.Matchers.equalTo;

public class DeleteActivitiesStepDefinition extends SetUp {

    private final HashMap<String,Object> headers=new HashMap<>();
    private static final String ROL_NAME= "Usuario";

    @Dado("que un usuario ingreso a la plataforma")
    public void queUnUsuarioIngresoALaPlataforma() {
        generalSetUp(ROL_NAME);
        headers.put("Content-type","application/json");
    }
    @Cuando("el usuario realice la petición de eliminar el registro de una actividad")
    public void elUsuarioRealiceLaPeticionDeEliminarElRegistroDeUnaActividad() {
        theActorInTheSpotlight().attemptsTo(
                doDelete().usingTheResource(RESOURCE_ACTIVITIES+"1")
                        .withHeaders(headers)
        );
    }
    @Entonces("el usuario podrá ver que se eliminó correctamente el registro de la actividad")
    public void elUsuarioPodraVerQueSeEliminoCorrectamenteElRegistroDeLaActividad() {
        theActorInTheSpotlight().should(
                seeThat("el código de respuesta",was(),equalTo(HttpStatus.SC_OK))
        );
    }

}
