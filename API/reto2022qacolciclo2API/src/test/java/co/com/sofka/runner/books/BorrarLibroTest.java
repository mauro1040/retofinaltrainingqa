package co.com.sofka.runner.books;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        snippets = CucumberOptions.SnippetType.CAMELCASE,
        features = {"src/test/resources/features/books/borrarlibro.feature"},
        glue = {"co.com.sofka.stepdefinition.books.delete"}
)

public class BorrarLibroTest {
}
