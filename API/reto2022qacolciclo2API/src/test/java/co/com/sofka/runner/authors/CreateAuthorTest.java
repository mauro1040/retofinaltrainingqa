package co.com.sofka.runner.authors;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        snippets =CucumberOptions.SnippetType.CAMELCASE,
        publish = true,
        glue={"co.com.sofka.stepdefinition.authors.createauthor"},
        features = {"src/test/resources/features/authors/createauthor/createAuthor.feature"}
)
public class CreateAuthorTest {
}
