package co.com.sofka.runner.books;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        snippets = CucumberOptions.SnippetType.CAMELCASE,
        features = {"src/test/resources/features/books/crearlibro.feature"},
        glue = {"co.com.sofka.stepdefinition.books.create"}
)

public class CrearLibroTest {
}
