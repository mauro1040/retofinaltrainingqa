package co.com.sofka.stepdefinition.books.delete;

import co.com.sofka.questions.common.ResponseCode;
import co.com.sofka.setup.book.SetUpBook;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import org.assertj.core.api.Assertions;

import static co.com.sofka.tasks.DoDelete.doDelete;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.hamcrest.Matchers.equalTo;

public class DeleteBookSD extends SetUpBook {

    @Dado("dado que el usuario esta en el recurso web indicando los datos del libro")
    public void dadoQueElUsuarioEstaEnElRecursoWebIndicandoLosDatosDelLibro() {
        try {
            generalSetUp();
        } catch (Exception e){
            LOGGER.error(e.getMessage(),e);
            Assertions.fail(e.getMessage());
        }
    }

    @Cuando("el usuario genera la eliminación")
    public void elUsuarioGeneraLaEliminacion() {
        try {
            actor.attemptsTo(
                    doDelete()
                            .usingTheResource(RESOURCE_BOOKS + "/2")
                            .withHeaders(headers)
            );
        } catch (Exception e){
            LOGGER.error(e.getMessage(),e);
            Assertions.fail(e.getMessage());
        }
    }

    @Entonces("el usuario visualizará un mensaje de confirmación")
    public void elUsuarioVisualizaraUnMensajeDeConfirmacion() {
        actor.should(
                seeThat(
                        ResponseCode.was(), equalTo(200)
                )
        );
    }

}
