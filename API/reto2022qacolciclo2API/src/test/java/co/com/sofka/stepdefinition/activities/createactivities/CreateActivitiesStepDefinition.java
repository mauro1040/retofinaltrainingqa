package co.com.sofka.stepdefinition.activities.createactivities;

import co.com.sofka.setup.SetUp;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import net.serenitybdd.screenplay.rest.questions.LastResponse;
import org.apache.http.HttpStatus;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;

import static co.com.sofka.questions.common.ResponseCode.was;
import static co.com.sofka.questions.common.ReturnStringValue.returnStringValue;
import static co.com.sofka.tasks.DoPost.doPost;
import static co.com.sofka.utils.FileUtilities.readFile;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;

public class CreateActivitiesStepDefinition extends SetUp {

    private final HashMap<String,Object> headers=new HashMap<>();
    private static final String ROL_NAME= "Usuario";
    private static final String CREATE_ACTIVITIES_JSON="src/test/resources/files/activities/createActivities.json";
    private String bodyRequest;

    @Dado("que un usuario ingreso a la plataforma y suministre los datos de la actividad")
    public void queUnUsuarioIngresoALaPlataformaYSuministreLosDatosDeLaActividad() {
        generalSetUp(ROL_NAME);
        headers.put("Content-type","application/json");
        bodyRequest=defineBodyRequest();
    }
    @Cuando("el usuario realice la petición de registro de la actividad")
    public void elUsuarioRealiceLaPeticionDeRegistroDeLaActividad() {
        theActorInTheSpotlight().attemptsTo(
                doPost().usingTheResource(RESOURCE_ACTIVITIES)
                        .withHeaders(headers)
                        .andBodyRequest(bodyRequest)
        );
    }
    @Entonces("el usuario podrá ver que se realizo el registro de la actividad")
    public void elUsuarioPodraVerQueSeRealizoElRegistroDeLaActividad() {
        String restResponse=new String(LastResponse.received().answeredBy(theActorInTheSpotlight()).asByteArray(), StandardCharsets.UTF_8);

        theActorInTheSpotlight().should(
                seeThat("el código de respuesta",was(),equalTo(HttpStatus.SC_OK))
        );

        theActorInTheSpotlight().should("La actividad registrada es un daily scrum",
                seeThat(returnStringValue(restResponse),containsString("Daily"))
        );
    }

    private String defineBodyRequest(){
        return readFile(CREATE_ACTIVITIES_JSON);
    }

}
