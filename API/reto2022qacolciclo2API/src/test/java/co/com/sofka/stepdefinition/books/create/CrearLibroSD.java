package co.com.sofka.stepdefinition.books.create;

import co.com.sofka.questions.common.ResponseCode;
import co.com.sofka.setup.book.SetUpBook;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import net.serenitybdd.screenplay.rest.questions.LastResponse;
import org.assertj.core.api.Assertions;

import java.nio.charset.StandardCharsets;

import static co.com.sofka.questions.common.ReturnStringValue.returnStringValue;
import static co.com.sofka.tasks.DoPost.doPost;
import static co.com.sofka.utils.FileUtilities.readFile;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;

public class CrearLibroSD extends SetUpBook {


    private static final String BODY_MESSAGE_JSON = "src/test/resources/files/books/crearlibro.json";

    @Dado("dado que el usuario se encuentra en el recurso web indicando los datos del libro")
    public void dadoQueElUsuarioSeEncuentraEnElRecursoWebIndicandoLosDatosDelLibro() {
        try {
            generalSetUp();
        } catch (Exception e){
            LOGGER.error(e.getMessage(),e);
            Assertions.fail(e.getMessage());
        }
    }

    @Cuando("el usuario genera el registro")
    public void elUsuarioGeneraElRegistro() {
        try {
            actor.attemptsTo(
                    doPost()
                            .usingTheResource(RESOURCE_BOOKS)
                            .withHeaders(headers)
                            .andBodyRequest(defineBodyMessageRequest())
            );
        } catch (Exception e){
            LOGGER.error(e.getMessage(),e);
            Assertions.fail(e.getMessage());
        }
    }

    @Entonces("el usuario visualizará su información de registro")
    public void elUsuarioVisualizaraSuInformacionDeRegistro() {
        String postResponse = new String(LastResponse.received().answeredBy(actor).asByteArray(), StandardCharsets.UTF_8);

        actor.should(
                seeThat(
                        ResponseCode.was(), equalTo(200)
                ),
                seeThat(
                        returnStringValue(postResponse),
                        containsString("\"title\":" + "\"Libro generico\"")
                )
        );

        LOGGER.info(postResponse);
    }

    private String defineBodyMessageRequest(){
        return readFile(BODY_MESSAGE_JSON);
    }

}