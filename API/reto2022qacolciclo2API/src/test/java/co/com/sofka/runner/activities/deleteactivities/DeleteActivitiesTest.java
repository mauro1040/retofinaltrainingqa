package co.com.sofka.runner.activities.deleteactivities;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        snippets =CucumberOptions.SnippetType.CAMELCASE,
        publish = true,
        glue={"co.com.sofka.stepdefinition.activities.deleteactivities"},
        features = {"src/test/resources/features/activities/deleteactivities/deleteActivities.feature"}
)
public class DeleteActivitiesTest{
}
