package co.com.sofka.runner.authors;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        snippets =CucumberOptions.SnippetType.CAMELCASE,
        publish = true,
        glue={"co.com.sofka.stepdefinition.authors.updateauthor"},
        features = {"src/test/resources/features/authors/updateauthor/updateAuthor.feature"}
)
public class UpdateAuthorTest {
}
