package co.com.sofka.setup.book;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.assertj.core.api.Assertions;

import java.util.HashMap;

import static co.com.sofka.utils.Log4jValues.LOG4J_PROPERTIES_FILE_PATH;

public class SetUpBook {

    public static final String URL_BASE="https://fakerestapi.azurewebsites.net/api/v1/";

    public static final String RESOURCE_BOOKS="Books/";

    protected static final Logger LOGGER= Logger.getLogger(SetUpBook.class);

    protected final Actor actor = new Actor("User");

    protected final HashMap<String, Object> headers = new HashMap<>();

    protected void generalSetUp(){
        try {
            setUpLog4j();
            setUpUser();
        }catch (Exception e){
            LOGGER.error(e.getMessage(),e);
            Assertions.fail(e.getMessage());
        }
    }

    protected void setUpLog4j(){
        PropertyConfigurator.configure(System.getProperty("user.dir")+LOG4J_PROPERTIES_FILE_PATH.getValue());
    }

    protected void setUpUser(){
        actor.can(CallAnApi.at(URL_BASE));
        headers.put("Content-Type", "application/json");
    }

}
