package co.com.sofka.runner.activities.createactivities;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        snippets =CucumberOptions.SnippetType.CAMELCASE,
        publish = true,
        glue={"co.com.sofka.stepdefinition.activities.createactivities"},
        features = {"src/test/resources/features/activities/createactivities/createActivities.feature"}
)
public class CreateActivitiesTest {

}
