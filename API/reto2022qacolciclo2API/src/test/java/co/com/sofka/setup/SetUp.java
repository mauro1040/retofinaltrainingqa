package co.com.sofka.setup;

import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.assertj.core.api.Assertions;
import static co.com.sofka.utils.Log4jValues.LOG4J_PROPERTIES_FILE_PATH;
import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;

public class SetUp {
    public static final String URL_BASE="https://fakerestapi.azurewebsites.net/api/v1/";

    public static final String RESOURCE_ACTIVITIES="Activities/";
    public static final String RESOURCE_AUTHORS="Authors/";

    public static final String RESOURCE_USERS="Users/";

    private static final Logger LOGGER= Logger.getLogger(SetUp.class);

    protected void generalSetUp(String name){
        try {
            setUpLog4j();
            setUpUser(name);
        }catch (Exception e){
            LOGGER.error(e.getMessage(),e);
            Assertions.fail(e.getMessage());
        }
    }

    protected void setUpLog4j(){
        PropertyConfigurator.configure(System.getProperty("user.dir")+LOG4J_PROPERTIES_FILE_PATH.getValue());
    }

    protected void setUpUser(String name){
        OnStage.setTheStage(new OnlineCast());
        theActorCalled(name).can(CallAnApi.at(URL_BASE));
    }

}
