# language: es

  Característica: Crear un autor
    Como editor
    necesito crear un autor
    para realizar seguimiento a sus actividades

  Escenario: Creación de registro autor de forma exitosa
    Dado que el editor navegó hasta la sección de registro de autor y suministre los datos
    Cuando el editor realice la petición de crear registro de autor
    Entonces visualizará que se creo correctamente el registro de autor