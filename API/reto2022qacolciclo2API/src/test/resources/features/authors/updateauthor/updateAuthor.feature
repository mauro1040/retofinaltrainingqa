# language: es

Característica: Actualizar registro de un autor
  Como editor
  necesito actualizar el registro de un autor
  para realizar una corrección en su registro

  Escenario: Actualizar registro autor de forma exitosa
    Dado que el editor navegó hasta la sección de actualización de registros de autor y suministre los datos
    Cuando el editor realice la petición de actualizar registro de autor
    Entonces visualizará que se creo actualizo correctamente el registro del autor