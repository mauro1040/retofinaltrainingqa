#language: es

Característica: Actualizaciones en el recurso de Users
  Como usuario administrador del sitio SwaggerUI
  necesito hacer uso del recurso de Users
  para elininar  o actualizar usuarios por su identificador

  Antecedentes:
    Dado que el administrador se encuentra usando el recurso Users del sitio SwaggerUI

  Escenario: Eliminar usuario por Id
    Cuando el administrador hace una solicitud para eliminar un usuario registrado por Id
    Entonces  el administrador recibira una confirmacion exitosa del proceso

  Escenario: Actualizar datos de un usuario registrado
    Cuando el administrador hace una solicitud para actualizar los datos de un usuario
    Entonces el administrador recibira confirmacion del proceso con los datos actualizados