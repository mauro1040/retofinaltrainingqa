#language: es

Característica: Borrar libro
    Como usuario
    necesito borrar un libro
    para eliminarlo del registro

    Escenario: Borrar libro
        Dado dado que el usuario esta en el recurso web indicando los datos del libro
        Cuando el usuario genera la eliminación
        Entonces el usuario visualizará un mensaje de confirmación