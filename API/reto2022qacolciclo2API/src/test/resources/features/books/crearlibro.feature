#language: es

Característica: Crear libro
    Como usuario
    necesito crear un libro
    para guardar su información

    Escenario: Crear libro
        Dado dado que el usuario se encuentra en el recurso web indicando los datos del libro
        Cuando el usuario genera el registro
        Entonces el usuario visualizará su información de registro