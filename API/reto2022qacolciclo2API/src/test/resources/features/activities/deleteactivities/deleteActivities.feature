#language: es

Característica: Eliminar registro de una actividad
  Como usuario de la plataforma
  necesito eliminar el registro una actividad
  para llevar un control sobre las actividades terminadas

  Escenario: Eliminar registro exitoso de una actividad
    Dado que un usuario ingreso a la plataforma
    Cuando el usuario realice la petición de eliminar el registro de una actividad
    Entonces el usuario podrá ver que se eliminó correctamente el registro de la actividad