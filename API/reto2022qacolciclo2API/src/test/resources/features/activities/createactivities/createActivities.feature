#language: es

    Característica: Crear registro de una actividad
        Como usuario de la plataforma
        necesito registrar una actividad
        para llevar un control sobre las actividades

    Escenario: Crear registro exitoso de una actividad
        Dado que un usuario ingreso a la plataforma y suministre los datos de la actividad
        Cuando el usuario realice la petición de registro de la actividad
        Entonces el usuario podrá ver que se realizo el registro de la actividad
